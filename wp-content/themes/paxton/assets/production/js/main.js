jQuery(function ($) {

	$(document).ready(function (e) {

		HeaderMenuFunction();

		GravityFormFunctionality();

		HomepageTitle();
		HomepageGetHeightofTitle();

		AddClassToggler();

		BurgerMenuFunctionality();

	})


	// All Header Options
	function HeaderMenuFunction(){

		// tell when to run the stuck
		var $offset = $('#masthead').attr('data-offset') == 'true' ? $('#masthead').height() + 10 : 30

		// Sticky header
		$(window).scroll(function () {
			if($(document).scrollTop() > $offset){
				$('body').addClass('header-stuck')
			}else{
				$('body').removeClass('header-stuck')
			}
		});

		// Burger menu toggle
		$(document).on('click', 'button.js-hamburger', function(e){
			e.preventDefault()
			$(this).toggleClass('is-active')
			$('body').toggleClass('mobile-menu-open')
			
			// hook so other function can runnig
			$(window).trigger('hamburger-menu-click')
		})

		// Disable parent menu to be clickable
		$('.main-navigation li.menu-item-has-children > a').on('click',function(e){
			e.preventDefault()
		})


		// Open close search
		$('.menu-search').click(function(e){
			e.preventDefault()
			$('body').toggleClass('search-open')
			$('.header-search-form .search-field').focus()

			// fullscreen style
			if($('.header-search-form').hasClass('fullscreen')){
				$('.header-search-form').fadeToggle(200,function(){
					// focus the search
					$('.header-search-form .search-field').focus()
				})
			}
		})

		// close fullscreen
		$('.fullscreen-shader').click(function(e){
			e.preventDefault()
			$('body').removeClass('search-open')
			$('.header-search-form').fadeOut(200)
		})

	}


	// Gravity Label Functionality
	function GravityFormFunctionalitySetup() {

		// label hover style
		$('.gfield input, .gfield textarea').on('change keyup focusout', function(){
			
			var isEmpty = $(this).val()
			if(isEmpty == ''){
				$(this).parents('.gfield').removeClass('filled')
			}else{
				$(this).parents('.gfield').addClass('filled')
			}

		})

		// trigger by default
		$('.gfield input, .gfield textarea').trigger('focusout')

		// label hover style
		$('.gfield input, .gfield textarea').on('focusin', function(){
			
			$(this).parents('.gfield').addClass('filled')
			
		})

		// Scroll the user to the top of the form if there is an error on this page
		if ($(".gform_wrapper.gform_validation_error").length > 0) {

			$('html, body').animate({
				scrollTop: $(".gform_wrapper").offset().top - 140
			}, 1);
		}

		// Setup this label for movement
		// Form choice placeholder
		$(".form-label-move").find(".ginput_container.ginput_container_text, .ginput_container.ginput_container_textarea, .ginput_container.ginput_container_email, .dn-form-input-container").each(function () {

			// Set this label up for greateness
			$(this).siblings("label").addClass("text-field");

			// On focusing of this elements
			$(this).find("input, textarea").focusin(function () {
				$(this).parent().siblings("label").addClass("focus");
			});

			// If any of these fields already have content
			$(this).find("input, textarea").each(function () {

				if ($(this).val() != '') {
					$(this).parent().siblings("label").addClass("focus");
				}
			});

			// On stop focusing of this elements
			$(this).find("input, textarea").focusout(function () {

				// If this field has no contents
				if ($(this).val() == '') {
					$(this).parent().siblings("label").removeClass("focus");
				}
			});

		});

		// Setup live form validation
		// Add required fields in place of grav forms classes
		$(".gfield_contains_required").find('input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], input[type="number"], input[type="tel"],  input[type="date"], input[type="month"], input[type="week"], input[type="time"], input[type="datetime"], input[type="datetime-local"], input[type="color"], textarea').addClass("required");

		// On unclicking a form field
		$("input.required:not(.ignore), textarea.required:not(.ignore), .gfield_contains_required select").focusout(function () {

			if ($(this).val() == '') {
				$(this).addClass("error");
			} else {
				$(this).removeClass("error");
				$(this).parents(".gfield.gfield_error").removeClass("gfield_error");
			}
		});
	}

	// service accordion
	$(document).on('click','.dn-services .service-item .heading:not(.disable-accordion)',function(e){
	
		e.preventDefault()
		$(this).parent().toggleClass('active')
		$(this).siblings('.content').slideToggle()

	})

	// slide menu
	// function SlideFullMenuFunctionality(){

	// 	if( $('.header-slide-full').length > 0 ){

	// 		var count = 0;
	// 		$('.header-slide-full .the-menu .menu > li').each(function(i,e){
	// 			$(this).addClass('delay-'+i)
	// 			count++
	// 		})

	// 		// the link
	// 		$('.header-slide-full .links').addClass('delay-'+ (count+1) )

	// 	}

	// }





	// Creates functionality for burger menus
	function BurgerMenuFunctionality() {

		if ($('#burger,#burger-blocky').length > 0) {

			var MenuOpenReady = true;

			$('#burger,#burger-blocky').click(function () {

				if (MenuOpenReady) {

					MenuOpenReady = false;
					setTimeout(function () {
						MenuOpenReady = true;
					}, 250);

					if ($('#burger,#burger-blocky').hasClass("open")) {
						CloseMenu();

					} else {
						OpenMenu();
					}
				}
			});

		}

	}

	function OpenMenu() {

		// Close the search
		CloseSearchDrop();

		// Close the side-cart
		CloseSideCart();

		$("#header-menu").addClass("appear");
		$("#masthead").addClass("filled-menu");
		$("body").addClass("mobile-menu-open");
		$(".hamburger-blush").addClass('is-active');
		$("#burger,#burger-blocky").addClass('open');
		ShowMenuShader();
		LockPageScroll();
	}

	function CloseMenu() {
		$("#header-menu").removeClass("appear");
		$("body").removeClass("mobile-menu-open");
		$("#masthead").removeClass("filled-menu");
		$(".hamburger-blush").removeClass('is-active');
		$("#burger,#burger-blocky").removeClass('open');
		HideMenuShader();
		UnlockPageScroll();
	}

	function GravityFormFunctionality() {

		// Run once on startup
		GravityFormFunctionalitySetup();

		// Run on failed sbumissions
		jQuery(document).bind('gform_post_render', function () {
			if ($(".validation_error").length > 0) {
				GravityFormFunctionalitySetup();
			}
		});
	}

	function HomepageTitle(){
		jQuery(document).ready(function( $ ) {
			HomepageGetHeightofTitle()
			$(window).resize(function(){
			   HomepageGetHeightofTitle()
			});
		});
	}

	function HomepageGetHeightofTitle(){
		jQuery(document).ready(function( $ ) {
		   var height_of_home_title = $(".main-banner-title").outerHeight();
		   if ( ($(this).width() > 991)  ) { // Desktop
		      $(".main-banner-content").css({
		      	"margin-top": (height_of_home_title ) + "px",
		      });
		   }else{
		   	$(".main-banner-content").css({
		   		"margin-top": "auto",
		   	});
		   }
		});
	}

	function AddClassToggler(){
		// init controller
		var controller = new ScrollMagic.Controller({globalSceneOptions: {duration: 500}});

		for (i = 1; i < 20; i++) {
		 new ScrollMagic.Scene({triggerElement: ".make-effect-" + i})
		 				.setClassToggle(".make-effect-" + i, "active") // add class toggle
		 				.addTo(controller);		  
		} 
	}
});