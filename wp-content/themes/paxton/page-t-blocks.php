<?php
/*
Template Name: Page Builder
 */
get_header(); ?>

<div class="site-content">
    
    <main id="main" class="site-main" >
        <?php while ( have_posts() ) : the_post(); ?>
            <article>

                <?php dn_page_hero(); ?>

				<?php page_builder_flex_content(); ?>
				
            </article>
            <?php dn_post_edit_link(); ?>
        <?php endwhile; // end of the loop. ?>
    </main>
 
</div>
<?php get_footer();