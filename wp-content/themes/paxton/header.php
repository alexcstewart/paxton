<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="page-wraper">

<?php 

	// Please change the scss file on \assets\production\sass\header\_header.scss
	// Use the correct scss

	include THEME_DIR . "/blocks/headers/menu-sidebar.php";

?>

<?php
	// Define whether the page need to be pushed down or not
	// Typically this is used when we have fullscreen hero element
	$hero_layout = get_field('hero_layout');
	$is_hero_fullscreen = get_field('hero_enabled') && $hero_layout == 'banner_large' ? 'no-padding' : '';
?>
<div id="content" class="page-content-wrapper <?php echo $is_hero_fullscreen ?>">
