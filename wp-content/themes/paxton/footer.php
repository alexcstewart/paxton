<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Digital_Noir_Starter_Pack
 */

?>

	</div><!-- #content -->

	<?php if(!(is_front_page() || is_home()) && !is_404()): ?>
	      <footer id="colophon" class="site-footer">
	   		<?php if(!is_page(667)): ?>
	   		   <div class="footer-socials">
	   		   	<div class="container">
	   		   		<div class="row">
	   		   		    <div class="col-12 col-md-12 text-center">
	   		   		        <h3 class="footer-socials-title">Stay connected</h3>
	   		   		        <?php # Template Part | Social Media
	   		   		        get_template_part('template-parts/content-social-media'); ?>
	   		   		    </div>
	   		   		</div>
	   		   	</div>
	   		   </div>
	   		<?php endif; ?>	
	   		<div class="footer-copyright">
		   		<div class="container-fluid">
		   			<div class="row">
		   			   <div class="col-12 copy-text col-md-4">
	   			         
	   			         	<?php
	   			         	    $footer_text = get_field('footer_text', 'options');
	   			         	    $footer_text = str_replace('[year]', date('Y'), $footer_text);
	   			         	    echo $footer_text;
	   			         	?>
	   			         
		   			   </div>
		   			   <div class="col-12 footer-link-menu col-md-4">
		   			      <?php wp_nav_menu( array(
		   			          'theme_location' => 'footer',
		   			      ) ); ?>
		   			   </div>
		   			   <div class="col-12 digital-noir col-md-4">
		   			     <a href="https://www.digital-noir.com" target="_blank" rel="nofollow"><i class="icon-DN_black"></i></a>
		   			   </div>
		   			</div>
		   		</div>
	   		</div>
	    	</footer><!-- #colophon -->	
	   <?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>