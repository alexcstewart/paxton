<?php
/*
Template Name: About Template
 */
get_header(); ?>

<div class="site-content">
    
    <main id="main" class="site-main" >
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="about-title"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <h2 class="team-main-title"><?php the_field('col_1_title'); ?></h2>
                        <div class="team-p"><?php the_field('col_1_content'); ?></div>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-6">
                                <h2 class="team-main-title"><?php the_field('col_2_title'); ?></h2>
                                <div class="hidden-md hidden-lg">
                                    <div class="image-team"><?php the_field('team_image'); ?></div>
                                </div>
                                <div class="team-p"><?php the_field('col_2_content'); ?></div>
                            </div>
                            <div class="col-12 col-md-6 visible-md visible-lg">
                                <div class="image-team"><?php the_field('team_image'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="project-quote">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-12">
                            <div class="blockquote-wrapper" >
                                <div class="project-blockquote-image" >
                                    <div class="team-bg-image"style="padding-left:50px"><?php the_field('quote_bg_image'); ?></div>
                                </div>
                                <div class="project-blockquote-container">
                                    <div class="project-blockquote">
                                        <blockquote>
                                            <p><?php the_field('quote_content'); ?></p>
                                        </blockquote>
                                        <div class="project-blockquote-subject">
                                            <p><?php the_field('quote_info'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
       <?php endwhile; // end of the loop. ?>
    </main>
 
</div>
<?php get_footer();