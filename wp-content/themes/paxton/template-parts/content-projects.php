<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Digital_Noir_Starter_Pack
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="single-project-title">
		<h1 class="single-project-title-child"><?php the_title() ?></h1>
	</div>

	<section class="section-project-style-1 ">
		<div class="container-fluid">
			<div class="row">
			    <div class="col-xs-11 col-md-10 col-md-offset-1">
			        <div class="section-project-image">
			        	  <?php $post_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
			        	  <img src="<?php echo $post_image; ?>" title="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>">
			        	  <div class="section-project-description">
			        	  	<p>This architecturally designed space completed in 2019 revitalised this seaside family home using a mixture of materials and finishes.</p>
			        	  </div>
			        </div> 
			    </div>
			</div>
		</div>
		
	</section>

	<section class="section-project-style-2 ">
		<div class="project-container">
			<div class="container-fluid">
				<div class="row">
				    <div class="col-xs-11 col-md-3 col-md-offset-1">
					    	<div class="project-style-2-content">
					    		<p>Here is a more detailed description of the overall project or the accompanying image.  It might look like a lot of text but it’s a pretty thin column so it’s not.</p>
					    		<p>If you have anything a little more wordy to say about your project, this would be the column to put it. Everything after this will be very light on text.</p>
					    	</div>
				    </div>
				    <div class="col-xs-11 col-md-8 remove-position">
				    		<img src="<?php echo wp_get_attachment_url(710); ?>" class="project-container-image" title="500x500" alt="500x500">
				    </div>
				</div>
			</div>
		</div>
	</section>

	<section class="section-project-quotation project-style">
		<div class="container-fluid">
			<div class="row">
			    <div class="col-xs-11 col-md-10 col-md-offset-1">
			    		<img src="<?php echo wp_get_attachment_url(718); ?>" class="col-md-8" title="500x500" alt="500x500">
			    		<div class="project-blockquote">
			    			<blockquote>
			    				<p>We are so thankful that we choose Paxton Constructions to renovate our bathroom and laundry .We could not be happier with the completed job. The work was undertaken in a professional and efficient manner. </p>
			    				<p>We were kept up to date with the progress and Shane was easily contactable with any questions we had at all stages of the renovations. The job came in on budget and schedule. I would highly recommend Paxton Constructions.</p>
			    				<p><strong>P + R / LARGS BAY</strong></p>
			    			</blockquote>
			    		</div>
			    </div>
			</div>
		</div>
	</section>

	<section class="section-project-style-2 project-style">
		<div class="project-container">
			<div class="container-fluid">
				<div class="row">
				    <div class="col-xs-11 col-md-3 col-md-offset-1">
					    	<div class="project-style-2-content">
					    		<p>Here is a more detailed description of the overall project or the accompanying image.  It might look like a lot of text but it’s a pretty thin column so it’s not.</p>
					    		<p>If you have anything a little more wordy to say about your project, this would be the column to put it. Everything after this will be very light on text.</p>
					    	</div>
				    </div>
				    <div class="col-xs-11 col-md-8 remove-position">
				    		<img src="<?php echo wp_get_attachment_url(711); ?>" class="project-container-image" title="500x500" alt="500x500">
				    </div>
				</div>
			</div>
		</div>
	</section>

	<section class="section-project-vertical">
		<div class="container-fluid">
			<div class="row">
			    <div class="col-xs-11 col-md-5 col-md-offset-1 ">
			        <div class="project-vertical vertical-going-right" style="background-image: url('<?php echo wp_get_attachment_url(719); ?>');"></div>
			    </div>
			    <div class="col-xs-11 col-md-5">
			        <div class="project-vertical vertical-going-left " style="background-image: url('<?php echo wp_get_attachment_url(718); ?>');"></div>
			    </div>
			</div>
		</div>
	</section>

	<section class="section-project-horizontal" style="background-image: url('<?php echo wp_get_attachment_url(720); ?>');" >

	</section>




	<footer class="entry-footer">
		<?php digital_noir_starter_pack_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
