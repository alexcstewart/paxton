<?php // shader ?>
<div id="menu-side-shader" style="display:none"></div>

<script>
    // consider to move this to project.js
    jQuery(window).on('hamburger-menu-click', function(){
        if(jQuery('body').hasClass('mobile-menu-open')){
            jQuery('#menu-side-shader').fadeIn(250)
        }else{
            jQuery('#menu-side-shader').fadeOut(250)
        }
    })

    // close menu
    jQuery('#menu-side-shader').click(function(){
        jQuery('button.js-hamburger').trigger('click')
    })

</script>

<?php 
    // Real header start here
?>
<header id="masthead" class="site-header menu-side">

    <div class="site-branding">
        <a href="<?php echo get_home_url(); ?>" class="special-link"><img src="<?php echo THEME_URL; ?>/img/header-logo.jpg" alt=""></a> 
    </div><!-- .site-branding -->

    <nav class="menu-side-main-navigation">
        <div class="hidden-print">
            <div id="menu-side-trigger" class="burger-menu">

                <div class="search-resp">
                    <a href="#" class="menu-search" title="Search"><i class="icon-search"></i></a>
                </div>

                <button class="hamburger hamburger--slider js-hamburger">
                    <span class="hamburger-box"><span class="hamburger-inner"></span></span>
                </button>

            </div>

            <div id="menu-side-resp">
                <?php include THEME_DIR . "/blocks/headers/menu-side-resp.php"; ?>
            </div>

        </div>
    </nav><!-- .menu-side-main-navigation -->

    <?php
        // Search Form - Padded
        // include THEME_DIR . "/blocks/headers/search-form-padded.php";

        // Search Form - Fullscreen
        include THEME_DIR . "/blocks/headers/search-form-fullscreen.php";
    ?>

</header><!-- #masthead -->