
<?php
    // blocky header content
    ob_start();
?>
    <div class="site-branding">
        <a href="<?php echo get_home_url(); ?>" class="special-link"><img src="<?php echo THEME_URL; ?>/img/header-logo.svg" alt=""></a> 
    </div><!-- .site-branding -->

   <nav class="menu-blocky-main-navigation hidden-sm hidden-xs">
       <div class="line-wrapper">
           <div class="hidden-print">
               <?php wp_nav_menu( array( 'theme_location' => 'header', 'menu_class'=> 'menu', 'depth' => 1 ) ); ?>
           </div>
       </div>
   </nav><!-- .menu-blocky-main-navigation -->

    <?php
        // Search Form - Padded
        // include THEME_DIR . "/blocks/headers/search-form-padded.php";

        // Search Form - Fullscreen
        //include THEME_DIR . "/blocks/headers/search-form-fullscreen.php";
    ?>
<?php $header_content = ob_get_clean(); ?>


<?php
    // This is for menu show when its scrolled
    // copy entire #masthead content
    // consider put this on footer
?>

<?php if(false): ?>
   <div class="site-header menu-blocky menu-on-scroll hidden-sm hidden-xs">
       <?php echo $header_content ?>
   </div>
<?php endif; ?>

<?php
    // This is the responsive menu
    // Always put this line in the footer.php before </body> tags
    // I put here so it's easily to manage
?>
<div class="hidden-lg hidden-print">
    <div id="menu-blocky-resp">
        <?php include THEME_DIR . "/blocks/headers/menu-blocky-resp.php"; ?>
    </div>
</div>


<?php 
    // Real header start here
?>
<header id="masthead" class="site-header menu-blocky" data-offset="true">
    <?php echo $header_content ?>
    <div class="burger-menu-blocky visible-xs visible-sm">
        <button class="hamburger hamburger--slider js-hamburger">
            <span class="hamburger-box"><span class="hamburger-inner"></span></span>
        </button>
    </div>
</header><!-- #masthead -->