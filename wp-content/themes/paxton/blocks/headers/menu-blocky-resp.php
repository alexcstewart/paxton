<div class="mobile-menu-wrapper">
    <div class="mobile-menu-holder">
        
        <?php wp_nav_menu( array( 'theme_location' => 'header', 'depth' => 1 ) ); ?>

        <div class="social-navigation">
            <?php echo dn_print_social_icon() ?>
        </div>

    </div><!-- .mobile-menu-holder -->
</div>