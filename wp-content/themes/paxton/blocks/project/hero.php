
<?php $hero_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'big'); ?>

<?php
    $layout = get_field('layout_options');
    $first_image = get_sub_field('image_one');
    $second_image = get_sub_field('image_two');
    $third_image = get_sub_field('image_three');
    $project_gallery_images = get_field('project_gallery_images');
    $project_type = get_field('project_type');
?>

<div class="single-project-title">
    <h1 class="single-project-title-child"><?php echo get_the_title(); ?></h1> 
</div>

<?php if ($project_type == "gallery_only"): ?>
    <?php foreach ($project_gallery_images as $gallery_row) : ?>
        <section class="<?php echo $gallery_row['layout_options']?>">
            <div class="container">
                
            </div>
        </section>
    <?php endforeach; ?>
<?php elseif ($project_type == "standard"): ?>
    <section class="project-hero">
        <div class="container">
            <div class="row">
                <div class="col-xs-11 col-md-12">
                    <div class="section-hero">
                        <div class="project-hero-image"style="background-image: url('<?php echo $hero_image[0]; ?>')"></div>
                        
                        <div class="short-intro-text-box">
                            <p class="short-intro-text">
                                <?php the_field('short_introduction');?>
                            </p>
                        </div>
                    </div>        
                </div> 
            </div>     
        </div>
    </section>

    <section class="featured-content">
        <div class="section-featured">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-push-4">
                        <div class="image-divider">
                            <div class="featured-image"><?php the_field('featured_image_1'); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </section>

    <section class="quotation-content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="project-blockquote-wrapper">
                        <div class="project-blockquote-image"><?php the_field('quotation_image'); ?> </div>
                        <div class="project-blockquote-container">
                            <div class="project-blockquote">    
                                <blockquote>
                                    <p><?php the_field('quotation_text'); ?></p>
                                </blockquote>
                                <div class="blockquote-subject"><?php the_field('quotation_title'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="featured-content">
        <div class="section-featured">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-push-4">
                        <div class="image-divider">
                            <div class="featured-image"><?php the_field('featured_image_2'); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </section>

    <section class="vertical-images">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-6">
                    <div class="vertical-image image-left-side">
                        <?php the_field('vertical_1'); ?>
                    </div>
                </div>
                <div class="col-xs-6 col-md-6">
                    <div class="vertical-image image-right-side">
                        <?php the_field('vertical_2'); ?>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="section-project-horizontal">
        <div><?php the_field('horizontal_1') ;?></div>
    </section>
<?php endif; ?>