<?php

// Based on the code fom this plugin
// https://wordpress.org/plugins/acf-focal-point/
class acf_field_focal_point extends acf_field {
	
	
	/*
	*  __construct
	*
	*  This function will setup the field type data
	*
	*  @type	function
	*  @date	5/03/2014
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	
	function __construct() {
		
		/*
		*  name (string) Single word, no spaces. Underscores allowed
		*/
		
		$this->name = 'focal_point';
		
		
		/*
		*  label (string) Multiple words, can include spaces, visible when selecting a field type
		*/
		
		$this->label = __('Focal Point', 'acf-focal_point');
		
		
		/*
		*  category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
		*/
		
		$this->category = 'jquery';
		
		
		/*
		*  defaults (array) Array of default settings which are merged into the field object. These are used later in settings
		*/
		
		$this->defaults = array(
			'save_format'	=>	'tag',
			'preview_size'	=>	'medium',
		);
		
		
		/*
		*  l10n (array) Array of strings that are used in JavaScript. This allows JS strings to be translated in PHP and loaded via:
		*  var message = acf._e('focal_point', 'error');
		*/
		
		$this->l10n = array();
		
				
		// do not delete!
    	parent::__construct();
    	
	}
	
	
	/*
	*  render_field_settings()
	*
	*  Create extra settings for your field. These are visible when editing a field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field_settings( $field ) {
		
		/*
		*  acf_render_field_setting
		*
		*  This function will create a setting for your field. Simply pass the $field parameter and an array of field settings.
		*  The array of settings does not require a `value` or `prefix`; These settings are found from the $field array.
		*
		*  More than one setting can be added by copy/paste the above code.
		*  Please note that you must also have a matching $defaults value for the field name (font_size)
		*/
		
		// Render return value radio
		acf_render_field_setting( $field, array(
			'label'			=> __('Return Value','acf-focal_point'),
			'instructions'	=> __('Specify the returned value on front end','acf-focal_point'),
			'type'			=> 'radio',
			'name'			=> 'save_format',
			'layout'		=>	'horizontal',
			'choices'		=> 	array(
				'object'		=>	__("Image Object",'acf'),
				'tag'			=>	__("Image Tag",'acf')
			)
		));
	}
	
	
	
	/*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field (array) the $field being rendered
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field( $field ) {

		// Merge defaults
		$field = array_merge($this->defaults, $field);
		
		// Get set image id
		$id = (isset($field['value']['id'])) ? $field['value']['id'] : '';


		// data vars
		$data = array(
			'top'		=>	isset($field['value']['top']) ? $field['value']['top'] : '',
			'left'		=>	isset($field['value']['left']) ? $field['value']['left'] : '',
			'right'		=>	isset($field['value']['right']) ? $field['value']['right'] : '',
			'bottom'	=>	isset($field['value']['bottom']) ? $field['value']['bottom'] : '',
		);
		

		
		// If we already have an image set...
		if ($id) {
			
			// Get image by ID, in size set via options
			$img = wp_get_attachment_image_src($id, $field['preview_size']);
						
		}
			
		// If image found...
		// Set to hide add image button / show canvas
		$is_active 	= ($id) ? 'active' : '';

		// And set src
		$url = ($id) ? $img[0] : '';
		
		
		// create Field HTML
		?>
		<div class="acf-focal_point <?php echo $is_active; ?>" data-preview_size="<?php echo $field['preview_size']; ?>">

			<input class="acf-focal_point-id" type="hidden" name="<?php echo $field['name']; ?>[id]" value="<?php echo $id; ?>" />

			<?php foreach ($data as $k => $d): ?>
				<input class="acf-focal_point-<?php echo $k ?>" type="hidden" name="<?php echo $field['name']; ?>[<?php echo $k ?>]" value="<?php echo $d ?>" />
			<?php endforeach ?>

			<div class="has-image" style="min-height:100px;min-width:150px;background:#eaeaea">
				<span class="acf-button-delete acf-icon -cancel acf-icon-cancel dark" data-name="remove"></span>
				<img class="acf-focal_point-image" src="<?php echo $url; ?>" />
				<canvas class="acf-focal_point-canvas"></canvas>
			</div>

			<div class="clear"></div>

			<div class="no-image">
				<p><?php _e('No image selected','acf'); ?> <input type="button" class="button add-image" value="<?php _e('Add Image','acf'); ?>" />
			</div>

		</div><?php
	}
	
	/*
	*  format_value()
	*
	*  This filter is appied to the $value after it is loaded from the db and before it is returned to the template
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value which was loaded from the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*
	*  @return	$value (mixed) the modified value
	*/
	
	function format_value( $value, $post_id, $field ) {

		// Merge defaults
		$field = array_merge($this->defaults, $field);

		// validate
		if( !$value ) {
			return false;
		}

		// Get image ID
		$image_ID = $value['id'];		

		
		if ($field['save_format'] == 'tag'){

			// print the image including <div ....
			return dn_get_background_image( $image_ID, $value['left'], $value['top'] );;

		}else{
			// print the array
			$return_object = array(
				'id' => $image_ID,
				'left' => $value['left'],
				'top' => $value['top'],
			);
			return $return_object;
		}
	
	}
	
	
}


// create field
new acf_field_focal_point();

// print background image in full size
function dn_get_background_image ( $image_ID, $focal_x = '', $focal_y = '', $size = 'image-1920') {

    if( $image_ID == '' || !is_numeric($image_ID) ){
        return;
	}	


	// Basic setup
    // Start with the oiriginal, large size image as a fallback
    $final_output = '<div class="lazyload bg-image bg-image-' . $image_ID . '"';

	// If the focal point is being used, apply the style directly onto the tag
    if ( $focal_x || $focal_y ) {

        $final_output .= ' style="';
        
        if ( $focal_x ) {
            $final_output .= "background-position-x:" . round($focal_x * 100) . "%;";
        }

        if ( $focal_y ) {
            $final_output .= "background-position-y:" . round($focal_y * 100) . "%;";
        }
        $final_output .= '"';
	}

	// ensure the screen set available, if not, just use the full image
	$scrset = wp_get_attachment_image_srcset($image_ID, $size);
	$source = wp_get_attachment_image_src($image_ID, 'full');
	$is_scrset_exists = $scrset != '' ? $scrset :  $source[0];

	$final_output .= ' data-bgset="' . $is_scrset_exists .'"';
	
	// Call an end to the bg-image tag
	$final_output .= '></div>';
	
	return $final_output;

}