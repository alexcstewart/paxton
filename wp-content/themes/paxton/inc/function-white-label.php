<?php

/* Set Default Function */
add_action( 'after_setup_theme', 'digital_noir_starter_pack_setup' );
function digital_noir_starter_pack_setup() {

	// Enable Post thumbnail and document title
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );

	// Registering Navigation Menu
	register_nav_menus( array(
		'header' => __( 'Header Menu', 'digitalnoir' ),
		'footer' => __( 'Footer Menu', 'digitalnoir' ),
	) );

	// Add HTML 5 support
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'digital_noir_starter_pack_content_width', 1140 );

}


/**
 * Register widget area.
 */
add_action( 'widgets_init', 'digitalnoir_widgets_init' );
function digitalnoir_widgets_init() {
	
	//register main sidebar
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'digitalnoir' ),
		'id'            => 'sidebar-main',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	
	// register footer widget
	register_sidebar( array(
		'name'          => __( 'Footer 1', 'digitalnoir' ),
		'id'            => 'footer-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 2', 'digitalnoir' ),
		'id'            => 'footer-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 3', 'digitalnoir' ),
		'id'            => 'footer-3',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 4', 'digitalnoir' ),
		'id'            => 'footer-4',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}


// CLEANING UP WP HEAD
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2 );
remove_action('wp_head', 'feed_links_extra', 3 );
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'rest_output_link_wp_head' );
remove_action('wp_head', 'wp_oembed_add_discovery_links' );
remove_action('wp_head', 'wp_oembed_add_host_js' );
remove_action('rest_api_init', 'wp_oembed_register_route');
remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
remove_action( 'wp_head', 'wp_resource_hints', 2 );

// REMOVE EMOJI
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// REMOVE COMMENT INLINE STLYE
add_action('widgets_init', 'dn_remove_recent_comments_style');
function dn_remove_recent_comments_style() {
    global $wp_widget_factory;
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}

// REMOVE JQUERY MIGRATE
add_action('wp_default_scripts', 'dn_remove_jquery_migrate');
function dn_remove_jquery_migrate($scripts){
    if (!is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];
        
        if ($script->deps) { // Check whether the script has any dependencies
            $script->deps = array_diff($script->deps, array(
                'jquery-migrate'
            ));
        }
    }
}

//tweaking wp-admin bar
add_action('wp_before_admin_bar_render', 'digitalnoir_admin_bar_tweak', 0);
function digitalnoir_admin_bar_tweak() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('wp-logo');
	$wp_admin_bar->remove_menu('comments');
	$wp_admin_bar->remove_menu('customize');
	$wp_admin_bar->remove_menu('themes');
	$wp_admin_bar->remove_menu('widgets');
	$wp_admin_bar->remove_menu('menus');	
}


// Add css to the login page form
add_action('login_head', 'digital_noir_custom_login');
function digital_noir_custom_login() {
    echo '<link rel="stylesheet" type="text/css" href="' . THEME_URL . '/admin.css" />';
}


// thanks for creating with digitalnoir
add_filter('admin_footer_text', 'digitalnoir_dashboard_footer');
function digitalnoir_dashboard_footer($footer) {
	echo 'A <a href="http://www.digital-noir.com">Digital Noir</a> Production';
}


// remove wp version for non admin
add_action( 'admin_menu', 'digitalnoir_remove_version' );
function digitalnoir_remove_version() {
    if ( ! current_user_can('manage_options') ) { // 'update_core' may be more appropriate
        remove_filter( 'update_footer', 'core_update_footer' ); 
    }
}


//remove dashboard widget
add_action('wp_dashboard_setup', 'digitalnoir_remove_dashboard_widget' );
function digitalnoir_remove_dashboard_widget() {
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
} 



// disable lost password from wp-login
add_action('login_head', 'dn_login_lost',0);
function dn_login_lost() {
	
	$curLost = isset($_GET['action']) && $_GET['action'] != '' ? $_GET['action'] : '';
	if($curLost == 'lostpassword'){
		exit();
	}
	
}


// client logo
add_action('login_head', 'dn_login_logo');
function dn_login_logo() {
    echo '<style type="text/css">
    body.login{ background-image: url('. THEME_URL .'/img/pattern.png);
        background-repeat:
        repeat;
        background-color: #fafafa;
    }
    body.login a{
        color:#fff
    }
    h1 a {
        background: url('. THEME_URL .'/img/logo-admin.png) no-repeat center !important;
        background-size:contain!important;
        width:100%!important
    }
    </style>';
}


// change logo link
add_filter('login_headerurl', 'dn_change_wp_login_url');
function dn_change_wp_login_url() {
	return '';
}


// remove forgot pass
add_filter( 'gettext', 'dn_remove_lostpassword_text' );
function dn_remove_lostpassword_text ( $text ) {
    if ($text == 'Lost your password?'){
            $text = '';
    }
    
    return $text;
}


// If woocommerce is not being used
if ( !class_exists( 'WooCommerce' ) ) {
    
    // change to login via email only
    remove_filter('authenticate', 'wp_authenticate_username_password', 20);
    add_filter('authenticate', 'dn_new_authentication', 20, 3);

}

// authenticate filter
function dn_new_authentication($user, $email, $password){

    //Check for empty fields
    if(empty($email) || empty ($password)){        
        //create new error object and add errors to it.
        $error = new WP_Error();

        if(empty($email)){ //No email
            $error->add('empty_username', __('<strong>ERROR</strong>: Email field is empty.'));
        }
        else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //Invalid Email
            $error->add('invalid_username', __('<strong>ERROR</strong>: Email is invalid.'));
        }

        if(empty($password)){ //No password
            $error->add('empty_password', __('<strong>ERROR</strong>: Password field is empty.'));
        }

        return $error;
    }

    //Check if user exists in WordPress database
    $user = get_user_by('email', $email);

    //bad email
    if(!$user){
        $error = new WP_Error();
        $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
        return $error;
    }
    else{ //check password
        if(!wp_check_password($password, $user->user_pass, $user->ID)){ //bad password
            $error = new WP_Error();
            $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
            return $error;
        }else{
            return $user; //passed
        }
    }
}

// change login using username text to email address
add_filter('gettext', 'dn_gettext_login', 20);
function dn_gettext_login($text){
    if( in_array($GLOBALS['pagenow'], array('wp-login.php'))){
        if( 'Username' == $text){
            return 'Email Address';
        }

		if( 'Username or Email' == $text || 'Username or Email Address' == $text){
            return 'Email Address';
        }
    }
    return $text;
}


//////////////////////////////////////////////////////////////////////
// ADD NEW PASSWORD TO THE EMAIL WHEN UPDATING THE USER VIA BACKEND //
//////////////////////////////////////////////////////////////////////

// send new password to email
add_filter( 'password_change_email', 'dn_send_new_password_email', 10, 3 );
function dn_send_new_password_email($pass_change_email, $user, $userdata){

	// get temporary plain password
	$user_id = $user['ID'];
	$plain_pass = get_user_meta( $user_id, '_dn_temporary_plain_pass', true );
	if($plain_pass != ''){

		$first_name = get_user_meta( $user_id, 'first_name', true );
		ob_start()
		?>
		
		<p>Hi <?php echo $first_name?>,</p>
		<p>Your password for the website ###SITENAME### site has been updated.</p>

		<p>Your new login details:</p>
        <p><strong>Email Address:</strong> ###EMAIL###</p>
        <p><strong>Password:</strong> <?php echo $plain_pass ?></p>

        <p>It's important to change passwords on your website regularly as a best practice for security. Regular password changes guarantee someone can't acquire your password over an extended period of time.</p>
        <p>If you'd like to change your password, you can edit it on your profile page (top right in website backend).</p>

		
		<p>- ###SITENAME###</p>

		<?php

		// the message
		$pass_change_email['message'] = ob_get_clean();

		// add date so client can easily find it on their email
		$original = $pass_change_email['subject'];
		$pass_change_email['subject'] = $original.' ['.date('Y-m-d').']';

		// remove the user meta
		delete_user_meta( $user_id, '_dn_temporary_plain_pass' );

	}


	return $pass_change_email;
}

// save temporary plain password
add_filter( 'send_password_change_email', 'dn_save_plain_pass_to_user_meta', 10 ,3);
function dn_save_plain_pass_to_user_meta($return = true, $user, $userdata){

	$user_id = $user['ID'];
	update_user_meta( $user_id, '_dn_temporary_plain_pass', $_REQUEST['pass1-text'] );	

	return $return;

}


// REORDER MENU BACKEND
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');
function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;
    
     
    return array(
        'index.php', // Dashboard
        'separator1', // First separator
        'edit.php?post_type=page', // Pages
        'edit.php', // Posts
        'edit.php?post_type=project', //Project
        'gf_edit_forms', // Form
        'upload.php', // Media
        'separator2', // Second separator
        'acf-options-core-options', // Core Options
        'link-manager.php', // Links
        
        //'edit-comments.php', // Comments
        
        //'themes.php', // Appearance
        'plugins.php', // Plugins
        'users.php', // Users
        //'tools.php', // Tools
        'options-general.php', // Settings
        'separator-last', // Last separator
    );
}

//////////////////////////////////
// Allow editor to change menu  //
//////////////////////////////////
$role_object = get_role( 'editor' );
$role_object->add_cap( 'edit_theme_options' );

// add menu to admin menu
add_action('admin_head', 'dn_hide_menu');
function dn_hide_menu() {

    if (current_user_can('editor')) {

        remove_submenu_page( 'themes.php', 'themes.php' ); // hide the theme selection submenu
        remove_submenu_page( 'themes.php', 'widgets.php' ); // hide the widgets submenu
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Ftools.php' ); // hide the customizer submenu
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Ftools.php&#038;autofocus%5Bcontrol%5D=background_image' ); // hide the background submenu


        // these are theme-specific. Can have other names or simply not exist in your current theme.
        remove_submenu_page( 'themes.php', 'yiw_panel' );
        remove_submenu_page( 'themes.php', 'custom-header' );
        remove_submenu_page( 'themes.php', 'custom-background' );

        remove_menu_page( 'tools.php'  );
        //remove_menu_page( 'edit.php' ); // remove post page
    }
}


//////////////////////////////////
// Allow editor to create users //
//////////////////////////////////

add_action( 'admin_init', 'digitalnoir_editor_manage_users', 0 );
function digitalnoir_editor_manage_users() {

    /////////////////////////////////////////////////////
    // Remove subscriber & contributor role by default //
    /////////////////////////////////////////////////////

    if( get_role('subscriber') ){
        remove_role( 'subscriber' );
    }
    if( get_role('contributor') ){
        remove_role( 'contributor' );
    }

	// APPLY THE RULES
	$isa_user_caps = new Digitalnoir_User_Caps();

		// CHECK IF THE CAPS ALREADY CHANGED
    if ( get_option( 'digitalnoir_add_cap_editor_once_privacy' ) != 'done' ) {
     
        // let editor manage users
        $edit_editor = get_role('editor'); // Get the user role
        $edit_editor->add_cap('edit_users');
        $edit_editor->add_cap('list_users');
        $edit_editor->add_cap('create_users');
        $edit_editor->add_cap('add_users');
        $edit_editor->add_cap('delete_users');
        $edit_editor->add_cap('promote_users');
        $edit_editor->add_cap('manage_privacy_options');
 
        update_option( 'digitalnoir_add_cap_editor_once_privacy', 'done' );
    }
}

class Digitalnoir_User_Caps {
 
   // Add our filters
    function __construct() {
        add_filter( 'editable_roles', array(&$this, 'editable_roles') );
        add_filter( 'map_meta_cap', array(&$this, 'map_meta_cap'), 10, 4 );
    }
    
  // Remove 'Administrator' from the list of roles if the current user is not an admin
  function editable_roles( $roles ){
    if( isset( $roles['administrator'] ) && !current_user_can('administrator') ){
      unset( $roles['administrator']);
    }
    return $roles;
  }
  // If someone is trying to edit or delete an
  // admin and that user isn't an admin, don't allow it
  function map_meta_cap( $caps, $cap, $user_id, $args ){
    switch( $cap ){
        case 'edit_user':
        case 'remove_user':
        case 'promote_user':
            if( isset($args[0]) && $args[0] == $user_id )
                break;
            elseif( !isset($args[0]) )
                $caps[] = 'do_not_allow';
            $other = new WP_User( absint($args[0]) );
            if( $other->has_cap( 'administrator' ) ){
                if(!current_user_can('administrator')){
                    $caps[] = 'do_not_allow';
                }
            }
            break;
        case 'delete_user':
        case 'delete_users':
            if( !isset($args[0]) )
                break;
            $other = new WP_User( absint($args[0]) );
            if( $other->has_cap( 'administrator' ) ){
                if(!current_user_can('administrator')){
                    $caps[] = 'do_not_allow';
                }
            }
            break;
        default:
            break;
    }
    return $caps;
  }
 
}

// Since customize.php is hidden through css, add extra class here so that it isn't hidden for admin
add_filter( "admin_body_class", "add_custom_class_customize_admin" );
function add_custom_class_customize_admin ( $classes ) {

	$classes = get_userdata( get_current_user_id() );
	return "roles-".$classes->roles[0];

}




/////////////////////
// Remove comments //
/////////////////////

// Disable support for comments and trackbacks in post types
add_action('admin_init', 'df_disable_comments_post_types_support');
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}

// Close comments on the front-end
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);
function df_disable_comments_status() {
	return false;
}

// Hide existing comments
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}


// Remove comments page in menu
add_action('admin_menu', 'df_disable_comments_admin_menu');
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}


// Redirect any user trying to access comments page
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}


// Remove comments links from admin bar
add_action('init', 'df_disable_comments_admin_bar');
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}


///////////////////////////////////////////////////////
// Move Yoast to bottom of the pages in the back-end //
///////////////////////////////////////////////////////
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
function yoasttobottom() {
	return 'low';
}

///////////////////////////////////////
// Allow editor to use Yoast settings /
///////////////////////////////////////
add_action( 'admin_init', 'add_theme_caps');
function add_theme_caps() {
    // gets the author role
    $role = get_role( 'editor' );

    // This only works, because it accesses the class instance.
    // would allow the author to edit others' posts for current theme only
    $role->add_cap( 'wpseo_manage_options' );

    // Allow grav form editing
    $role->add_cap( 'gform_full_access' );
}


/** Add privacy capabilities to the related groups of WordPress built-in capabilities
 *  Hooked to 'ure_built_in_wp_caps' filter from URE_Capabilities_Groups_Manager
 * 
 * @param array $caps
 * @return array
 */
add_filter('ure_built_in_wp_caps', 'add_privacy_caps_to_groups', 10, 1);
function add_privacy_caps_to_groups($caps) {
    
    $caps['manage_privacy_options'] = array('core', 'general');
    $caps['export_others_personal_data'] = array('core', 'general');
    $caps['erase_others_personal_data'] = array('core', 'general');
    
    $admin_role = get_role('editor');    
    if (isset($admin_role->capabilities['manage_privacy_options'])) {
        return $caps;        
    }
    
    // add privacy caps to 'editor' role
    $roles = wp_roles();
    $old_use_db = $roles->use_db;
    $roles->use_db = true;
    $admin_role = get_role('editor');
    $admin_role->add_cap('manage_privacy_options', true);
    $admin_role->add_cap('export_others_personal_data', true);
    $admin_role->add_cap('erase_others_personal_data', true);
    $roles->use_db = $old_use_db;            

    return $caps;
}

// helper to allow editor edit privacy policy 
add_filter('map_meta_cap', 'map_for_privacy_caps', 10, 2);
function map_for_privacy_caps($caps, $cap) {

    // if admin just return
    $users = wp_get_current_user();
    $user_roles = $users->roles;
    if(in_array('administrator', $user_roles)){
        return $caps;
    }

    $privacy_caps = array('manage_privacy_options', 'export_others_personal_data', 'erase_others_personal_data');
    if (!in_array($cap, $privacy_caps)) {
        return $caps;
    }
    
    $default_cap = is_multisite() ? 'manage_network' : 'manage_options';        
    foreach ($caps as $key => $value) {
        if ($value == $default_cap) {
            unset($caps[$key]);
            break;
        }
    }
    $caps[] = $cap;
    
    return $caps;
}


/////////////////////////////////////
// Hide the Wordpress theme editor //
/////////////////////////////////////

add_action('admin_menu', 'wpr_remove_editor_menu', 1);
function wpr_remove_editor_menu() {
    
    // Removes this hook here
    // https://developer.wordpress.org/reference/functions/_add_themes_utility_last/
    remove_action('admin_menu', '_add_themes_utility_last', 101);
    
}


///////////////////////////////////////////
// Disable the Gutenberg editor on pages //
///////////////////////////////////////////
add_filter( 'use_block_editor_for_post_type', 'digital_noir_post_type_filter', 10, 2 );
function digital_noir_post_type_filter( $use_block_editor, $post_type ) {
    
	if ( $post_type === "page" ) {
		return false;
	}
    return $use_block_editor;
    
}


// helper check email domain
function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

// instruction page
add_action('admin_menu', 'dn_instruction_create_menu');
function dn_instruction_create_menu() {
    $current_user = wp_get_current_user();
    $user_email = $current_user->user_email;
	if(endsWith($user_email,'digitalnoir.com.au')){
		//create new top-level menu
		add_menu_page('Instruction Settings', 'Instruction Settings', 'administrator', 'digital-noir-backend', 'dn_instruction_settings_page' , 'dashicons-welcome-learn-more' );
		//call register settings function
		add_action( 'admin_init', 'register_dn_instruction_settings' );
	}
}

function register_dn_instruction_settings() {
	register_setting( 'digital-noir-setting-group', 'dashboard_instruction' );
}

function dn_instruction_settings_page() {
?>
<div class="wrap">
	<h2>Digitalnoir Setting</h2>
	<form method="post" action="options.php">
		<?php settings_fields( 'digital-noir-setting-group' ); ?>
		<?php do_settings_sections( 'digital-noir-setting-group' ); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row">Dashboard Instruction</th>
				<td><?php
					$instruction_setting = get_option('dashboard_instruction') ;
				 wp_editor($instruction_setting,'dashboard_instruction'); ?></td>
			</tr>
		</table>
		<?php submit_button(); ?>
	</form>
</div>
<?php }

// show the instruction
function dashboard_widget_function( $post, $callback_args ) {
	
	echo wpautop(get_option('dashboard_instruction'));

}

// Function used in the action hook
add_action('wp_dashboard_setup', 'add_dashboard_widgets' );
function add_dashboard_widgets() {
	wp_add_dashboard_widget('digital_noir_instruction_metabox', 'Resources for Website Changes', 'dashboard_widget_function');
}

// remove welcome
remove_action('welcome_panel', 'wp_welcome_panel');


// maintenance page
add_action( 'wp', 'digital_noir_maintenance_page' );
function digital_noir_maintenance_page() {
    if ( file_exists( ABSPATH . '.maintenance' ) ) {
        include_once THEME_DIR . '/maintenance.php';
        die();
    }
}

// disable wp admin bar
add_filter('show_admin_bar', '__return_false');


// disable gravatar for better performance
add_filter('get_avatar', 'digitalnoir_remove_gravatar', 1, 5);
function digitalnoir_remove_gravatar($avatar, $id_or_email, $size, $default) {
    return '';
}
 
