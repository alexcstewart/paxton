<?php

/////////////////////////////////////////
/////// W3 TOTAL CACHE TWEAK - FILE NOT REGENERATED //////
/////////////////////////////////////////
add_filter('w3tc_minify_urls_for_minification_to_minify_filename', 'digitalnoir_filename_filter', 20, 3);
function digitalnoir_filename_filter($minify_filename, $files, $type ){
    
    $ver = sanitize_title( str_replace('.','', THEME_VERSION) );
    
    $minify_filename = $ver.$minify_filename;
    
    return $minify_filename;
    
}

/////////////////////////////////////////
////////////////// Page Title ///////////
/////////////////////////////////////////
add_filter( 'wp_title', 'digitalnoir_wp_title', 10, 2 );
function digitalnoir_wp_title( $title, $sep ) {
    if ( is_feed() ) {
        return $title;
    }

    global $page, $paged;

    // Add the blog name
    $title .= get_bloginfo( 'name', 'display' );

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title .= " $sep $site_description";
    }

    // Add a page number if necessary:
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title .= " $sep " . sprintf( __( 'Page %s', 'digitalnoir' ), max( $paged, $page ) );
    }

    return $title;
}

/////////////////////////////////////////
// Extra Search icon on the main menu //
/////////////////////////////////////////
add_filter( 'wp_nav_menu_items','dn_primary_navigation_add_search_button_filter', 10, 2 );
function dn_primary_navigation_add_search_button_filter( $items, $args ){   
    
    $_menus = explode(' ', $args->menu_class );
    
    if( in_array('has-search', $_menus) )  {

        $items .=  '<li class="hidden-xs hidden-sm"><a class="menu-search" href="#" title="Search"><i class="icon-search"></i></a></li>';  
     
    }
         
    return $items;
}

/////////////////////////////////////////
// Extra Search icon on the main menu //
/////////////////////////////////////////
add_filter( 'wp_nav_menu_items','dn_primary_navigation_add_social_filter', 10, 2 );
function dn_primary_navigation_add_social_filter( $items, $args ){    

    $_menus = explode(' ', $args->menu_class );
    
    if( in_array('has-social', $_menus) )  {

        $items .=  '<li class="social-wrapper">
                    <div class="social-link">
                        '. dn_print_social_icon() .'
                    </div>
        </li>';  
     
    }
         
    return $items;
}




/////////////////////////////////////////
// Show edit button on the left bottom //
/////////////////////////////////////////
function dn_post_edit_link(){

    if( function_exists('WC') ){
        if( is_cart() || is_checkout() ){
            return;
        }
    }
    
    edit_post_link();

}


/////////////////////////////////////////
//////////// Gravity forms //////////////
/////////////////////////////////////////

// disabled page scroll after submission
add_filter( 'gform_confirmation_anchor', '__return_false' );

// error validation
add_filter( 'gform_validation_message', 'dn_grav_validation_message', 10, 2 );
function dn_grav_validation_message( $message, $form ) {
    return "<div class='validation_error dn-form-validation-error'><i class='icon-plus'></i> Oops, something went wrong. Please check highlighted input field.</div> ";
}

// Gravity forms confirmation message
add_filter( 'gform_confirmation', 'dn_grav_confirmation_message', 10, 4 );
function dn_grav_confirmation_message ( $confirmation, $form, $entry, $ajax ) {
    
    // If this is not a redirect
    if(is_array($confirmation)){
        if ( !array_key_exists( "redirect", $confirmation ) ) {
            $confirmation = "<div class='dn-form-confimration'><i class='icon-paperplane'></i> ".$form['confirmation']['message']."</div>";
        }
    }
    
    return $confirmation;
}

// ADD CLASS TO GRAVITY LI.FIELD
add_filter( 'gform_field_css_class', 'dn_gravity_custom_selector', 10, 3 );
function dn_gravity_custom_selector( $classes, $field, $form ) {
    
    $classes .= ' type-'.$field->type;

    return $classes;
}


/////////////////////////////////////////
////////////    Excerpt    //////////////
/////////////////////////////////////////

// Adds trailing ... to the excpert
add_filter( 'excerpt_more', 'custom_excerpt_more' );
function custom_excerpt_more( $more ) {
	return '...';
}

// excerpt length
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}


//////////////////////////
// Social sharing links //
//////////////////////////

// based on the infomration here
// https://github.com/bradvin/social-share-urls#facebook

function get_facebook_social_share ($url = "") {
    if ( $url == "" ) {
        $url = get_permalink();
    }
    $link = "https://www.facebook.com/sharer.php?u=" .$url."&p=";
    return "<a onclick='window.open(" . '"' . $link . '", "_blank", "width=500,height=500"' . ");' href='' target='_blank' rel='nofollow' class='special-link' ><i class='icon-facebook'></i></a>";
}
function get_twitter_social_share ($url = "") {
    
    if ( $url == "" ) {
        $url = get_permalink();
    }

    $link = "https://twitter.com/intent/tweet?url=" . $url;
    return "<a onclick='window.open(" . '"' . $link . '", "_blank", "width=500,height=500"' . ");' href='' target='_blank' rel='nofollow' class='special-link' ><i class='icon-twitter'></i></a>";
}
function get_linkedin_social_share ($url = "") {
    
    if ( $url == "" ) {
        $url = get_permalink();
    }

    $link = "https://www.linkedin.com/shareArticle?url=" . $url;
    return "<a onclick='window.open(" . '"' . $link . '", "_blank", "width=500,height=500"' . ");' href='' target='_blank' rel='nofollow' class='special-link' ><i class='icon-linkedin'></i></a>";
}
function get_email_social_share ($url = "") {
    
    if ( $url == "" ) {
        $url = get_permalink();
    }

    $link = "mailto:?subject=". get_bloginfo('name') .": " . get_the_title() . "&amp;body=Check out this site " . $url;
    return "<a href='".$link."' class='special-link'><i class='icon-paperplane'></i></a>";
}

// Combine All
function the_social_share_suite($url = "") {
    echo get_facebook_social_share($url);
    echo get_twitter_social_share($url);
    echo get_linkedin_social_share($url);
    echo get_email_social_share($url);
}


//////////////////////////
/////  Breadcrumbs  //////
//////////////////////////
function digitalnoir_breadcrumbs( $args = array() ) {

    // Do not display on the homepage
    if ( is_front_page() ) {
        return;
    }

    // Set default arguments
    $defaults = array(
        'separator_icon'      => '&raquo;',
        'breadcrumbs_id'      => 'breadcrumbs',
        'breadcrumbs_classes' => 'breadcrumb-trail breadcrumbs',
        'home_title'          => 'Home'
    );

    // Parse any arguments added
    $args = apply_filters( 'ct_ignite_breadcrumbs_args', wp_parse_args( $args, $defaults ) );

    // Set variable for adding separator markup
    $separator = '<span class="separator"> ' . esc_attr( $args['separator_icon'] ) . ' </span>';

    // Get global post object
    global $post;

    /***** Begin Markup *****/

    // Open the breadcrumbs
    $html = '<div id="' . esc_attr( $args['breadcrumbs_id'] ) . '" class="' . esc_attr( $args['breadcrumbs_classes']) . '">';

    // Add Homepage link & separator (always present)
    $html .= '<span class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . esc_attr( $args['home_title'] ) . '">' . esc_attr( $args['home_title'] ) . '</a></span>';
    $html .= $separator;

    // Post
    if ( is_singular( 'post' ) ) {
        
        // check if blog as page is set
        $is_blog_page_exist = get_option( 'page_for_posts' ) != '' ? true : false;
        if($is_blog_page_exist){
                $html .= '<span class="item-cat"><a class="bread-blog" href="' . get_permalink(get_option( 'page_for_posts' )) . '" title="Blog">Blog</a></span>';
                $html .= $separator;;
        }

        // Get post category info
        $category = get_the_category();
        
        $category = array_filter($category, function($obj){
                if(isset($obj->term_id)) {
                            if ($obj->term_id == 1) return false;
                }
                return true;
        });		
        if(!empty($category)):
        // Get category values
        $category_values = array_values( $category );

        // Get last category post is in
        $last_category = end( $category_values );

        // Get parent categories
        $cat_parents = rtrim( get_category_parents( $last_category->term_id, true, ',' ), ',' );

        // Convert into array
        $cat_parents = explode( ',', $cat_parents );

        // Loop through parent categories and add to breadcrumb trail
        foreach ( $cat_parents as $parent ) {
            $html .= '<span class="item-cat">' . wp_kses( $parent, wp_kses_allowed_html( 'a' ) ) . '</span>';
            $html .= $separator;
        }
        
        // end if not empty
        endif;

        // add name of Post
        $html .= '<span class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></span>';
    } // Page
    elseif ( is_singular( 'page' ) ) {

        // if page has a parent page
        if ( $post->post_parent ) {

            // Get all parents
            $parents = get_post_ancestors( $post->ID );

            // Sort parents into the right order
            $parents = array_reverse( $parents );

            // Add each parent to markup
            foreach ( $parents as $parent ) {
                $html .= '<span class="item-parent item-parent-' . esc_attr( $parent ) . '"><a class="bread-parent bread-parent-' . esc_attr( $parent ) . '" href="' . get_permalink( $parent ) . '" title="' . get_the_title( $parent ) . '">' . get_the_title( $parent ) . '</a></span>';
                $html .= $separator;
            }
        }
        // Current page
        $html .= '<span class="item-current item-' . $post->ID . '"><span title="' . get_the_title() . '"> ' . get_the_title() . '</span></span>';
    } // Attachment
    elseif ( is_singular( 'attachment' ) ) {

        // Get the parent post ID
        $parent_id = $post->post_parent;

        // Get the parent post title
        $parent_title = get_the_title( $parent_id );

        // Get the parent post permalink
        $parent_permalink = get_permalink( $parent_id );

        // Add markup
        $html .= '<span class="item-parent"><a class="bread-parent" href="' . esc_url( $parent_permalink ) . '" title="' . esc_attr( $parent_title ) . '">' . esc_attr( $parent_title ) . '</a></span>';
        $html .= $separator;

        // Add name of attachment
        $html .= '<span class="item-current item-' . $post->ID . '"><span title="' . get_the_title() . '"> ' . get_the_title() . '</span></span>';
    } // Custom Post Types
    elseif ( is_singular() ) {

        // Get the post type
        $post_type = get_post_type();

        // Get the post object
        $post_type_object = get_post_type_object( $post_type );

        // Get the post type archive
        $post_type_archive = get_post_type_archive_link( $post_type );

        // Add taxonomy link and separator
        $html .= '<span class="item-cat item-custom-post-type-' . esc_attr( $post_type ) . '"><a class="bread-cat bread-custom-post-type-' . esc_attr( $post_type ) . '" href="' . esc_url( $post_type_archive ) . '" title="' . esc_attr( $post_type_object->labels->name ) . '">' . esc_attr( $post_type_object->labels->name ) . '</a></span>';
        $html .= $separator;

        // Add name of Post
        $html .= '<span class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . $post->post_title . '">' . $post->post_title . '</span></span>';
    } // Category
    elseif ( is_category() ) {
        
        // check if blog as page is set
        $is_blog_page_exist = get_option( 'page_for_posts' ) != '' ? true : false;
        if($is_blog_page_exist){
                $html .= '<span class="item-cat"><a class="bread-blog" href="' . get_permalink(get_option( 'page_for_posts' )) . '" title="Blog">Blog</a></span>';
                $html .= $separator;;
        }

        // Get category object
        $parent = get_queried_object()->category_parent;

        // If there is a parent category...
        if ( $parent !== 0 ) {

            // Get the parent category object
            $parent_category = get_category( $parent );

            // Get the link to the parent category
            $category_link = get_category_link($parent);

            // Output the markup for the parent category item
            $html .= '<span class="item-parent item-parent-' . esc_attr( $parent_category->slug ) . '"><a class="bread-parent bread-parent-' . esc_attr( $parent_category->slug ) . '" href="' . esc_url( $category_link ) . '" title="' . esc_attr( $parent_category->name ) . '">' . esc_attr( $parent_category->name ) . '</a></span>';
            $html .= $separator;
        }

        // Add category markup
        $html .= '<span class="item-current item-cat"><span class="bread-current bread-cat" title="' . $post->ID . '">' . single_cat_title( '', false ) . '</span></span>';
    } // Tag
    elseif ( is_tag() ) {
        
        // check if blog as page is set
        $is_blog_page_exist = get_option( 'page_for_posts' ) != '' ? true : false;
        if($is_blog_page_exist){
                $html .= '<span class="item-cat"><a class="bread-blog" href="' . get_permalink(get_option( 'page_for_posts' )) . '" title="Blog">Blog</a></span>';
                $html .= $separator;;
        }

        // Add tag markup
        $html .= '<span class="item-current item-tag"><span class="bread-current bread-tag">' . single_tag_title( '', false ) . '</span></span>';
    } // Author
    elseif ( is_author() ) {
        
        // check if blog as page is set
        $is_blog_page_exist = get_option( 'page_for_posts' ) != '' ? true : false;
        if($is_blog_page_exist){
                $html .= '<span class="item-cat"><a class="bread-blog" href="' . get_permalink(get_option( 'page_for_posts' )) . '" title="Blog">Blog</a></span>';
                $html .= $separator;;
        }

        // Add author markup
        $html .= '<span class="item-current item-author"><span class="bread-current bread-author">' . get_queried_object()->display_name . '</span></span>';
    } // Day
    elseif ( is_day() ) {
        
        // check if blog as page is set
        $is_blog_page_exist = get_option( 'page_for_posts' ) != '' ? true : false;
        if($is_blog_page_exist){
                $html .= '<span class="item-cat"><a class="bread-blog" href="' . get_permalink(get_option( 'page_for_posts' )) . '" title="Blog">Blog</a></span>';
                $html .= $separator;;
        }

        // Add day markup
        $html .= '<span class="item-current item-day"><span class="bread-current bread-day">' . get_the_date() . '</span></span>';
    } // Month
    elseif ( is_month() ) {
        
        // check if blog as page is set
        $is_blog_page_exist = get_option( 'page_for_posts' ) != '' ? true : false;
        if($is_blog_page_exist){
                $html .= '<span class="item-cat"><a class="bread-blog" href="' . get_permalink(get_option( 'page_for_posts' )) . '" title="Blog">Blog</a></span>';
                $html .= $separator;;
        }

        // Add month markup
        $html .= '<span class="item-current item-month"><span class="bread-current bread-month">' . get_the_date( 'F Y' ) . '</span></span>';
    } // Year
    elseif ( is_year() ) {
        
        // check if blog as page is set
        $is_blog_page_exist = get_option( 'page_for_posts' ) != '' ? true : false;
        if($is_blog_page_exist){
                $html .= '<span class="item-cat"><a class="bread-blog" href="' . get_permalink(get_option( 'page_for_posts' )) . '" title="Blog">Blog</a></span>';
                $html .= $separator;;
        }

        // Add year markup
        $html .= '<span class="item-current item-year"><span class="bread-current bread-year">' . get_the_date( 'Y' ) . '</span></span>';
    } // Custom Taxonomy
    elseif ( is_archive() ) {

        // get the name of the taxonomy
        $custom_tax_name = get_queried_object()->name;

        // Add markup for taxonomy
        $html .= '<span class="item-current item-archive"><span class="bread-current bread-archive">' . esc_attr($custom_tax_name) . '</span></span>';
    } // Search
    elseif ( is_search() ) {
        
        // check if blog as page is set
        $is_blog_page_exist = get_option( 'page_for_posts' ) != '' ? true : false;
        if($is_blog_page_exist){
                $html .= '<span class="item-cat"><a class="bread-blog" href="' . get_permalink(get_option( 'page_for_posts' )) . '" title="Blog">Blog</a></span>';
                $html .= $separator;;
        }

        // Add search markup
        $html .= '<span class="item-current item-search"><span class="bread-current bread-search">Search results for: ' . get_search_query() . '</span></span>';
    } // 404
    elseif ( is_404() ) {

        // Add 404 markup
        $html .= '<span>' . __('Error 404', 'ignite') . '</span>';
    } // blog when not on homepage
    elseif ( is_home() ) {
        $html .= '<span>' . get_the_title( get_option('page_for_posts' ) ) . '</span>';
    }

    // Close breadcrumb container
    $html .= '</div>';

    $html = apply_filters( 'ct_ignite_breadcrumbs_filter', $html );

    echo wp_kses_post( $html );
}


///////////////////////////////////////
/////  Detect Internet Explorer  //////
///////////////////////////////////////
add_filter('body_class', 'is_ie_body_class', 10, 2);
function is_ie_body_class($classes, $class){
    if (preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']) || (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false)) {
        $classes[] = 'ie-browser';
    }

    return $classes;
}

////////////////////////////////////////////
// Product complex flex content container //
////////////////////////////////////////////

function page_builder_flex_content() {

    if( have_rows('page_builder') ):

        while ( have_rows('page_builder') ) : the_row();

            $layout = get_row_layout();
            $is_file = get_stylesheet_directory() . '/blocks/builder/'. $layout .'.php';
            $template = locate_template('blocks/builder/'. $layout .'.php');
            if( is_file( $is_file ) ){
                include( $template );
            }else{
                echo "\r\n <!-- /themes/blocks/builder/$layout.php is missing --> \n\r";
            }

        endwhile;

    endif;

}

////////////////////////////////////////////
///////// DN Enqueue Scripts ///////////////
////////////////////////////////////////////
global $dn_enqueue_style;
$dn_enqueue_style = array();
function dn_enqueue_style( $filename = '', $preload = true ){

    // its a must
    if($filename == ''){
        return;
    }

    global $dn_enqueue_style;

    // delete duplicated
    if(in_array($filename, $dn_enqueue_style)) {
        return;
    }

    echo "<link rel='stylesheet' id='dn-". $filename ."-css'  href='". THEME_URL ."/assets/dist/css/". $filename .".css?ver=". THEME_VERSION ."' type='text/css' media='all' />";

    $dn_enqueue_style[] = $filename;
}

////////////////////////////////////////////
///////// Lazy size attachment image ///////
////////////////////////////////////////////
function dn_get_attachment_image_lazy($image_ID, $size = 'image-1024') {

    if( $image_ID == '' || !is_numeric($image_ID) ){
        return;
	}	


	// Basic setup
    // Start with the oiriginal, large size image as a fallback
    $final_output = '<img class="lazyload attachment-' . $image_ID . '"';


	// ensure the screen set available, if not, just use the full image
	$scrset = wp_get_attachment_image_srcset($image_ID, $size);
	$source = wp_get_attachment_image_src($image_ID, 'full');
	$is_scrset_exists = $scrset != '' ? $scrset :  $source[0];

    $final_output .= ' data-src="' . $source[0] .'" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" alt=""';
    $final_output .= ' data-srcset="' . $is_scrset_exists .'"';
    $final_output .= ' data-sizes="auto" ';
	
	// Call an end to the bg-image tag
	$final_output .= '/>';
	
	return $final_output;


}

/**
 * Generate html link
 *
 * @param string $name ACF field name
 * @param string $class optional css class
 * @param bool $post_id usually overloaded as string 'options'
 */
function render_link_helper($name, $class = '', $post_id = false) {
    if ($post_id) {
        $link = get_sub_field($name, $post_id);
    } else {
        $link = get_sub_field($name);
    }

    if ($link) {
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
        echo "<a class='$class' href='" . esc_url($link_url) . "' target=" . esc_attr($link_target) . "'>" . esc_html($link_title) . "</a>";
    }
}

/**
 * Generate html image
 *
 * @param string $name ACF field name
 * @param string $class optional css class
 * @param bool $post_id usually overloaded as string 'options'
 */
function render_image_helper($name, $class = '', $post_id = false)
{
    if ($post_id) {
        $image = get_field($name, $post_id);
    } else {
        $image = get_field($name);
    }

    if($image['alt']){
        $image['alt'] = $image['title'];
    }

    if (!empty($image)) {
        echo "<img class='img-fluid $class' src=" . esc_url($image['url']) . " alt=" . esc_attr($image['alt']) . " />";
    }
}

////////////////////////////////////////////
///////// Social Link //////////////////////
////////////////////////////////////////////
function dn_print_social_icon(){
   ob_start();
   ?>
        <?php if ( get_field("facebook_link", "options") ) { ?><a href="<?php the_field("facebook_link", "options"); ?>" target="_blank" rel="nofollow" class="special-link"><i class="icon-facebook"></i></a><?php } ?>
        <?php if ( get_field("twitter_link", "options") ) { ?><a href="<?php the_field("twitter_link", "options"); ?>" target="_blank" rel="nofollow" class="special-link"><i class="icon-twitter"></i></a><?php } ?>
        <?php if ( get_field("instagram_link", "options") ) { ?><a href="<?php the_field("instagram_link", "options"); ?>" target="_blank" rel="nofollow" class="special-link"><i class="icon-instagram"></i></a><?php } ?>
        <?php if ( get_field("linkedin_link", "options") ) { ?><a href="<?php the_field("linkedin_link", "options"); ?>" target="_blank" rel="nofollow" class="special-link"><i class="icon-linkedin"></i></a><?php } ?>
        <?php if ( get_field("google_plus_link", "options") ) { ?><a href="<?php the_field("google_plus_link", "options"); ?>" target="_blank" rel="nofollow" class="special-link"><i class="icon-google-plus"></i></a><?php } ?>
        <?php if ( get_field("youtube_link", "options") ) { ?><a href="<?php the_field("youtube_link", "options"); ?>" target="_blank" rel="nofollow" class="special-link"><i class="icon-youtube"></i></a><?php } ?>
    <?php
    return ob_get_clean();
}

/////////////////////////////////////////////////////////////
/////// Search, Archive Page show all found post ////////////
/////////////////////////////////////////////////////////////
add_action('pre_get_posts', 'dn_search_archive_show_all', 1);
function dn_search_archive_show_all($query) {
	if (is_admin() || !$query->is_main_query()) {
		return;
	}
	if (is_search()) {
		$query->set('posts_per_page', -1);
		return;
	}
}


////////////////////////////////////////////
/////////// Page Hero //////////////////////
////////////////////////////////////////////
function dn_page_hero(){

    $hero_layout = get_field('hero_layout');
    $is_enabled = get_field('hero_enabled');
    
    if( $is_enabled ){

        if( $hero_layout == 'title_only' ){

            // default page title
            get_template_part('blocks/hero/default');

        }elseif( $hero_layout == 'banner_short' ){

            // half screen blocks
            get_template_part('blocks/hero/short-banner');

        }elseif( $hero_layout == 'banner_large' ){

            // full screen blocks
            get_template_part('blocks/hero/large-banner');

        }
        
    }else{

        // default page title
        get_template_part('blocks/hero/default');

    }

}

/////////////////////////////////////////////////////////////
/////// Render SVG from file ////////////
/////////////////////////////////////////////////////////////
function dn_get_svg( $filename ){

	$file = get_template_directory().'/img/'. $filename;

	if(file_exists($file) ){
        // tweak localhost
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        ); 
        
		$_file = get_template_directory(). '/img/'. $filename;
		return file_get_contents( $_file , true );

	}else{
		return '***image not found****';
	}

}