<?php

/////////////////////////////////////////
//////// Add Couple Image Size //////////
/////////////////////////////////////////
add_image_size( "image-2560", 2560, 99999, false ); // no crop
add_image_size( "image-1920", 1920, 99999, false ); // no crop
add_image_size( "image-1600", 1600, 99999, false ); // no crop
add_image_size( "image-1366", 1366, 99999, false ); // no crop
add_image_size( "image-1024", 1024, 99999, false ); // no crop
add_image_size( "image-768", 768, 99999, false ); // no crop
add_image_size( "image-640", 640, 99999, false ); // no crop
add_image_size( "image-520", 500, 99999, false ); // no crop
add_image_size( "image-420", 400, 99999, false ); // no crop


// Remove default image sizes here, we dont' use so often
add_filter( 'intermediate_image_sizes_advanced', 'prefix_remove_default_images', 999, 1 );
function prefix_remove_default_images( $sizes ) {

    unset( $sizes['thumbnail']); // 150px
    unset( $sizes['small']); // 150px
    unset( $sizes['large']); // 1024px
    unset( $sizes['medium_large']); // 768px
    
    return $sizes;
}

///////////////////////////////////////////////////
//////// Register admin.js and admin.css //////////
///////////////////////////////////////////////////
add_action( 'admin_enqueue_scripts', 'dn_load_custom_wp_admin_style' );
function dn_load_custom_wp_admin_style() {
    
    // style
    wp_enqueue_style( 'dn-wp-admin-css', THEME_URL . '/admin.css', false, THEME_VERSION  );
    
    // javascript
	wp_register_script( 'js-theme-admin', THEME_URL . '/assets/dist/js/admin.js', array('jquery', 'acf-input'), 1.0, true );	
	wp_localize_script( 'js-theme-admin', 'theme_var',
        array(
            'upload' => THEME_URL.'/img/acf-thumbnail/',
        )
	);
	wp_enqueue_script( 'js-theme-admin');
}


//////////////////////////////////////
// ACF - Create custom options page //
//////////////////////////////////////
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
        'page_title' => 'Core Options',
        'autoload' => false, // make less SQL query
    ));
	
}

//////////////////////////////////
// ACF field sync (through git) //
//////////////////////////////////
add_filter('acf/settings/load_json', 'dn_acf_json_load_point');
function dn_acf_json_load_point( $paths ) {
    
    // remove original path (optional)
    unset($paths[0]);
    
    // append path
    $paths[] = get_stylesheet_directory() . '/acf-json';
    
    // return
    return $paths;
    
}

//////////////////////////////////////////////////////////
// ACF - Change the default text for the Add Row button //
//////////////////////////////////////////////////////////
add_filter( 'gettext', 'dn_change_default_add_repeater', 20, 3 );
function dn_change_default_add_repeater( $translated_text, $text, $domain ) {

    if( $domain == 'acf') {

        switch ( $translated_text ) {
            case 'Add Row' :
            $translated_text = __( 'Add +', 'acf' );
            break;
        }
    }

    return $translated_text;
}

//////////////////////////////////////////////////////////
// ACF - Register Maps API //
//////////////////////////////////////////////////////////
add_filter('acf/init', 'dn_acf_google_map_api');
function dn_acf_google_map_api( $api ){

	acf_update_setting('google_api_key', GOOGLE_MAP_KEY);
	
}

//////////////////////////////////////////////////////////
// ACF - Add Video Link to Gallery Image //
//////////////////////////////////////////////////////////
add_filter("attachment_fields_to_edit", "dn_acf_attachment_field_to_edit", null, 2);
function dn_acf_attachment_field_to_edit($form_fields, $post) {
    // $form_fields is a special array of fields to include in the attachment form
    // $post is the attachment record in the database
    //     $post->post_type == 'attachment'
    // (attachments are treated as posts in Wordpress)
     
    // add our custom field to the $form_fields array
    // input type="text" name/id="attachments[$attachment->ID][gallery_video]"
    $form_fields["gallery_video"] = array(
        "label" => __("Youtube Video URL"),
        "input" => "text", // this is default if "input" is omitted
        "value" => get_post_meta($post->ID, "_gallery_video", true)
    );
    // if you will be adding error messages for your field, 
    // then in order to not overwrite them, as they are pre-attached 
    // to this array, you would need to set the field up like this:
    $form_fields["gallery_video"]["label"] = __("Youtube Video URL");
    $form_fields["gallery_video"]["input"] = "text";
	$form_fields["gallery_video"]["value"] = get_post_meta($post->ID, "_gallery_video", true);	
	
    return $form_fields;
}

//////////////////////////////////////////////////////////
// ACF - Add Video Link to Gallery Image - on save post //
//////////////////////////////////////////////////////////
add_filter('attachment_fields_to_save', 'dn_acf_attachment_field_to_save', 10, 2);
function dn_acf_attachment_field_to_save($post, $fields){

	$attachment_id = $post['ID'];
	if($fields['gallery_video'] != ''){
		update_post_meta($attachment_id, "_gallery_video", $fields['gallery_video']);	
	}

	return $post;	

}

/////////////////////////////////////////
////// WP Editor - TINYMCE Tweak ////////
/////////////////////////////////////////

// add class to link
add_action('admin_head','dn_tinymce_wp_link_classes', 1, 10);
function dn_tinymce_wp_link_classes( $args ){

    ?>
    <script>
    jQuery( document ).ready( function( $ ) {
        $('#link-options').append( 
            '<div style="padding-top:10px">'+ 
                '<label><span>Link Style</span>'+
                '<select name="wpse-link-class" id="wpse_link_class">'+
                    '<option value="none">None</option>'+
                    '<option value="dn-button blue">Button Blue</option>'+
                    '<option value="dn-button red">Button Red</option>'+
            '</select>'+
            '</label>'+
        '</div>' );
        
        if($('#link-options').length > 0){
                wpLink.getAttrs = function() {
                    wpLink.correctURL();        
                    return {
                        class:      $( '#wpse_link_class' ).val(),
                        href:       $.trim( $( '#wp-link-url' ).val() ),
                        target:     $( '#wp-link-target' ).prop( 'checked' ) ? '_blank' : ''
                    };
                }
            }
    });</script>
    <style>
    .has-text-field #wp-link .query-results {
        top: 250px!important;
    }
    </style>
    <?php

}

// Remove Heading 1 from editor
add_filter( 'tiny_mce_before_init', 'dn_mce_before_init_insert_formats' ); 
function dn_mce_before_init_insert_formats( $init_array ) {  
	
    $init_array['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Preformatted=pre;';
    
	return $init_array;  
	
} 



// Hook several function to admin init
add_action( 'admin_init', 'my_theme_add_editor_styles' );
function my_theme_add_editor_styles() { 

    // add custom style to wp-editor
    add_editor_style( 'custom-editor-style.css' );    

    // disable gravity form css
    update_option( 'rg_gforms_disable_css', 1 ); 
    
}

// Add table button to tinymce
add_filter( 'mce_buttons', 'dn_add_the_table_button' );
add_filter( 'mce_external_plugins', 'dn_add_the_table_plugin', 9999, 1 );

function dn_add_the_table_button( $buttons ) {
	array_push( $buttons, 'table' );
	return $buttons;
 }

function dn_add_the_table_plugin( $plugins ) {
    $plugins['table'] = get_stylesheet_directory_uri() . '/assets/dist/js/tinymce.js';
	return $plugins;
}

// small editor acf
add_action('admin_head', 'acf_small_editor_admin_styles');
function acf_small_editor_admin_styles() {
  
	?>
	<style>
    .small-editor .acf-editor-wrap iframe {			
			min-height: 0;
            height:180px!important
		}
	</style>
	<?php
	
}