<?php

/*
    Usage:
    This file contain project speficic page that may useful for 
    next project, to maintain this sustainability please don't 
    include this file directly, pick any function that suit to your
    need to related file back-end or frontend.

    Also please explain as detailed as possible to use the function
*/


/* 
    This function is to replace the acf radio field with icon/image, so when you call get_field it will straight output the icon/image
    This function scan the folder /themes/your-themes/img/accordion-image/ *** .svg
    - field_5dca47e69766d is the key for the acf field
    - the style on how it's appear on the backend can be editable on admin.scss
*/
add_filter('acf/load_field/key=field_5dca47e69766d', 'dn_acf_load_field');
function dn_acf_load_field( $field ) {

    $files = glob( THEME_DIR . '/img/accordion-image/*.{svg}', GLOB_BRACE);
    
    foreach($files as $file) {
        $filename = basename($file);
        $field['choices'][$filename] = '<img src="'. THEME_URL .'/img/accordion-image/'. $filename .'" alt="" height="40"/>';
    }

    return $field;
    
}