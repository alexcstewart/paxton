<?php
/*
Template Name: Contact Template
 */
get_header(); ?>

<div class="site-content">
    
    <main id="main" class="site-main" >
        <?php while ( have_posts() ) : the_post(); ?>
            <article>
                <div class="entry-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="entry-title">Contact</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <div class="main-content">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="content-form-sidebar">
                                <h2 class="sidebar-title">
                                    Our <br> Details
                                </h2>
                                <p>Paxton Constructions</p>
                                <p>
                                    <a href="tel:0439505154" target="_blank" class="special-link">0439 505 154 </a><br>
                                    <a href="mailto:admin@paxtonconstructions.com.au" target="_blank" class="special-link">admin@paxtonconstructions.com.au</a>
                                </p>
                                <div class="footer-socials-container">
                                    <a href="https://www.facebook.com/paxtonconstructions/" target="_blank" class="each-social" title="Facebook">
                                        <div class="each-social-parent">
                                            <div class="social-icon">
                                                <svg data-name="Group 185" xmlns="http://www.w3.org/2000/svg" width="18.872" height="35.523" viewBox="0 0 18.872 35.523">
                                                    <path  data-name="Path 309" d="M114.485.247A43.836,43.836,0,0,0,109.678,0c-5.159,0-8.364,3.288-8.364,8.582v4.1H96.442a.444.444,0,0,0-.444.444V19.28a.444.444,0,0,0,.444.444h4.871V35.079a.444.444,0,0,0,.444.444h6.357a.444.444,0,0,0,.444-.444V19.724h4.861a.444.444,0,0,0,.44-.387l.791-6.157a.444.444,0,0,0-.44-.5h-5.652V9.192c0-1.655.416-2.552,2.609-2.552h3.259a.444.444,0,0,0,.444-.444V.687A.444.444,0,0,0,114.485.247Zm-.5,5.5h-2.815c-3.082,0-3.5,1.739-3.5,3.441v3.931a.444.444,0,0,0,.444.444h5.591l-.677,5.269h-4.914a.444.444,0,0,0-.444.444V34.635H102.2V19.28a.444.444,0,0,0-.444-.444H96.886V13.567h4.871a.444.444,0,0,0,.444-.444V8.582c0-4.817,2.795-7.693,7.476-7.693,1.839,0,3.478.113,4.3.2V5.75Z" transform="translate(-95.998)"/>
                                                </svg>
                                            </div>
                                        </div>  
                                    </a>
                                    <a href="" target="_blank" class="each-social" title="Instagram">
                                        <div class="each-social-parent">
                                            <div class="social-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="35.523" height="35.523" viewBox="0 0 35.523 35.523">
                                                    <g>
                                                    <g>
                                                        <path d="M25.366,0H10.157A10.169,10.169,0,0,0,0,10.157V25.366A10.169,10.169,0,0,0,10.157,35.523H25.366A10.169,10.169,0,0,0,35.523,25.366V10.157A10.169,10.169,0,0,0,25.366,0Zm9.269,25.366a9.28,9.28,0,0,1-9.269,9.269H10.157A9.28,9.28,0,0,1,.888,25.366V10.157A9.28,9.28,0,0,1,10.157.888H25.366a9.28,9.28,0,0,1,9.269,9.269V25.366Z"/>
                                                    </g>
                                                    </g>
                                                    <g transform="translate(8.382 8.173)">
                                                    <g>
                                                        <path d="M106.3,94.234a9.656,9.656,0,1,0,9.656,9.656A9.667,9.667,0,0,0,106.3,94.234Zm0,18.424a8.768,8.768,0,1,1,8.768-8.768A8.778,8.778,0,0,1,106.3,112.658Z" transform="translate(-96.645 -94.234)"/>
                                                    </g>
                                                    </g>
                                                    <g transform="translate(25.479 4.175)">
                                                    <g>
                                                        <path d="M296.151,48.133a2.371,2.371,0,1,0,2.371,2.371A2.374,2.374,0,0,0,296.151,48.133Zm0,3.854a1.483,1.483,0,1,1,1.483-1.483A1.485,1.485,0,0,1,296.151,51.987Z" transform="translate(-293.78 -48.133)"/>
                                                    </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-8">
                            <div class="main-content-form">
                                <h2 class="gravity-section-title">
                                    Get<br> in Touch
                                </h2>
                                <?php $form = get_field('form'); 
                                gravity_form( $form['id'], $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, $tabindex, $echo = true ); ?> 
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php dn_post_edit_link(); ?>
        <?php endwhile; // end of the loop. ?>
    </main>
 
</div>
<?php get_footer('contact');