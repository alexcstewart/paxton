<?php
/*
Template Name: Our Projects
 */
get_header(); ?>
<?php dn_enqueue_style('page-project'); ?>

<div class="site-content">

    <main id="main" class="site-main" >
        <?php
        # Parameter
        $project_args = array (
            'post_type' => array( 'project', ),
            'posts_per_page'  => get_option('posts_per_page'),  # -1 for all
            'order'   => 'ASC',  # Newest
            'orderby' => 'menu_order',  # 'rand' 'post__in'
        );
        
        # Connect Loop to Parameter
        $project_query = new WP_Query( $project_args );
        ?>
        
        <?php
        # Loop
        if ( $project_query->have_posts() ) : ?>
        
            <div class="container project-container">
                <div class="row">
                    <div class="col-12 col-md-12 ">
                        <header class="entry-header">
                            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                        </header><!-- .entry-header -->
                       <?php $ctr = 1; ?>
                       <?php while ( $project_query->have_posts() ) : $project_query->the_post(); ?>
                            <?php $post_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
                            <div class="each-project make-effect-<?php echo $ctr; ?> <?php echo (( $ctr % 2 == 1 ) ? 'style-one ' : 'style-two'); ?> ">
                                <div class="row">
                                    <div class="col-12 col-md-3 <?php echo (( $ctr % 2 == 1 ) ? 'col-md-push-9 ' : ''); ?>">
                                        <div class="project-detail">
                                            <div class="row">
                                                    <div class="col-xs-3 project-left col-md-12 ">
                                                        <div class="project-counter">
                                                    <?php echo "0". $ctr ."." ; ?>
                                                </div>
                                                </div>
                                                <div class="col-xs-9 project-right col-md-12">
                                                    <h2><?php echo get_the_title(); ?></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 <?php echo (( $ctr % 2 == 1 ) ? 'col-md-pull-3' : ''); ?>  ">
                                        <a href="<?php echo get_permalink(); ?>" class="special-link each-project-wrapper <?php echo (( $ctr % 2 == 0 ) ? 'project-style-2' : 'project-style-1'); ?>">
                                            <div class="project-border"></div>
                                            <div class="each-project-image" style="background-image: url('<?php echo $post_image; ?>');" title="<?php echo get_the_title(); ?>"></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php $ctr++; ?>
                       <?php endwhile; ?> 
                    </div>
                </div>  
            </div>
        
            <?php wp_reset_query(); ?>

        <?php endif; ?>
    </main>
 

</div>
<?php get_footer();