<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Digital_Noir_Starter_Pack
 */

get_header(); ?>

<div class="site-content site-search">
	<?php dn_enqueue_style('search-results') ?>
	<section id="content" class="content-area">
		<main id="main" class="site-main" >

			<section class="intro">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="text-content">
								<?php
									global $wp_query;
									$number_of_search_results = $wp_query->found_posts;
									$plural = $number_of_search_results > 1 ? 'results' : 'result'; 
								?>
								<h1 class="h2"><?php echo $number_of_search_results; ?> <?php echo $plural ?> found for the keyword "<?php echo get_search_query(); ?>"</h1>
								<p>If you didn't find what you were looking for, try a new search!</p>
							</div>
							<?php get_search_form(); ?>
						</div>
					</div>
				</div>
			</section>

			<section class="results">
				<div class="container">
					<div class="row">
							<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part('blocks/blogs/loop-1-column'); ?>
							<?php endwhile; ?>
							
							<?php else : ?>
								<div class="col-xs-12">
									<h2>No search results found!</h2>
								</div>
							<?php endif; ?>
						
					</div>
				</div>
			</section>
			
		</main>
	</section>
</div>
<?php get_footer(); ?>
