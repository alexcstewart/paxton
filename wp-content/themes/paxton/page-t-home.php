<?php
/*
Template Name: Home
 */
get_header(); ?>
<?php dn_enqueue_style('page-home'); ?>

<div class="site-content">
    
    <main id="main" class="site-main" >
        <?php $acf_title = get_field('title', get_the_ID()); ?>
        <?php $acf_content = get_field('content', get_the_ID()); ?>
        <?php $acf_image = get_field('image', get_the_ID()); ?>
        <?php $button_link = get_field('button', get_the_ID()); ?>
        <?php $button_link_title = $button_link['title']; ?>
        <?php $button_link_url = $button_link['url']; ?>
        <?php $button_link_target = $button_link['target']; ?>
        
        <div class="home-wrapper home-wrapper-left">
            <div class="main-banner-parent">
                <div class="main-banner-child">
                    <?php if($acf_title): ?>
                       <h1 class="main-banner-title"><?php echo $acf_title; ?></h1>
                    <?php endif; ?>
                    <div class="main-banner-content">
                        <div class="teaser--home">
                            <?php echo wpautop($acf_content); ?>
                        </div>
                        <?php if($button_link): ?>
                           <a href="<?php echo $button_link_url; ?>" class="dn-button main-banner-button homepage-button pax-btn" tabindex="0"><?php echo $button_link_title; ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div> 
        </div>
        <div class="home-wrapper home-wrapper-right" >
            <?php the_field('image') ?>
        </div>
    </main>

    <?php if(false): ?>
       <div class="container-fluid h-100">
           
        <div class="row h-100">
            <div class="col-12 col-md-3 col-md-offset-1  h-100">
                
            </div>
            <div class="col-12 col-md-8 h-100">
                 
            </div>
        </div>
       </div>
    <?php endif; ?>
 
</div>
<?php get_footer();