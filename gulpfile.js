var gulp = require("gulp");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var sourcemaps = require("gulp-sourcemaps");
var uglify = require("gulp-uglify");
var pipeline = require("readable-stream").pipeline;
var concat = require("gulp-concat");
var themeURL = './wp-content/themes/paxton/';

gulp.task("style-fe", function() {
  // Styles
  return gulp
    .src( themeURL + "assets/production/sass/style.scss")
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: "compressed"
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer(
        "last 2 version",
        "safari 5",
        "ie 7",
        "ie 8",
        "ie 9",
        "opera 12.1",
        "ios 6",
        "android 4"
      )
    )
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(themeURL));
});

gulp.task("style-header", function() {
  // Styles
  return gulp
    .src(themeURL +"assets/production/sass/header/*.scss")
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: "compressed"
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer(
        "last 2 version", 
        "safari 5",
        "ie 7",
        "ie 8",
        "ie 9",
        "opera 12.1",
        "ios 6",
        "android 4"
      )
    )
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(themeURL +"assets/dist/css"));
});

gulp.task("style-builder", function() {
  // Styles
  return gulp
    .src(themeURL +"assets/production/sass/builder/*.scss")
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: "compressed"
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer(
        "last 2 version", 
        "safari 5",
        "ie 7",
        "ie 8",
        "ie 9",
        "opera 12.1",
        "ios 6",
        "android 4"
      )
    )
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(themeURL +"assets/dist/css"));
});

gulp.task("style-page", function() {
  // Styles
  return gulp
    .src(themeURL +"assets/production/sass/page/*.scss")
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: "compressed"
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer(
        "last 2 version", 
        "safari 5",
        "ie 7",
        "ie 8",
        "ie 9",
        "opera 12.1",
        "ios 6",
        "android 4"
      )
    )
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(themeURL +"assets/dist/css"));
});

gulp.task("style-plugins", function() {
  // Styles
  return gulp
    .src(themeURL +"assets/production/sass/plugins/*.scss")
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: "compressed"
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer(
        "last 2 version", 
        "safari 5",
        "ie 7",
        "ie 8",
        "ie 9",
        "opera 12.1",
        "ios 6",
        "android 4"
      )
    )
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(themeURL +"assets/dist/css"));
});

gulp.task("style-be", function() {
  // Admin
  return gulp
    .src(themeURL +"assets/production/sass/admin.scss")
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: "compressed"
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer(
        "last 2 version",
        "safari 5",
        "ie 7",
        "ie 8",
        "ie 9",
        "opera 12.1",
        "ios 6",
        "android 4"
      )
    )
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(themeURL));
});

gulp.task("js-fe", function() {
  return pipeline(
    gulp.src(themeURL +"assets/production/js/*.js"),
    uglify(),
    //concat("js-fe.min.js"),
    gulp.dest(themeURL +"assets/dist/js")
  );
});

gulp.task("js-plugins", function() {
  return pipeline(
    gulp.src(themeURL +"assets/production/js/plugins/*.js"),
    uglify(),
    gulp.dest(themeURL +"assets/dist/js")
  );
});


// Tasks for developing dev website
gulp.task("dev", function() {
  gulp.watch(
    themeURL +"assets/production/sass/**/*.scss",
    gulp.series("style-fe", "style-be")
  );

  gulp.watch(
    themeURL +"assets/production/sass/header/*.scss",
    gulp.series("style-header")
  );

  gulp.watch(
    themeURL +"assets/production/sass/builder/*.scss",
    gulp.series("style-builder")
  );

  gulp.watch(
    themeURL +"assets/production/sass/page/*.scss",
    gulp.series("style-page")
  );

  gulp.watch(
    themeURL +"assets/production/sass/plugins/*.scss",
    gulp.series("style-plugins")
  );

  gulp.watch(
    themeURL +"assets/production/js/*.js",
    gulp.series("js-fe")
  );

  gulp.watch(
    themeURL +"assets/production/js/plugins/*.js",
    gulp.series("js-plugins")
  );

  
});
