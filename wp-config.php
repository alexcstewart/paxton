<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'paxton_2021' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'vq<p(q}yh)zR<K`:8M7Yu);aS5rOa%sh7WmH0KRNcqb2LCKA_=_6IM9qPyH8oK4y' );
define( 'SECURE_AUTH_KEY',  'j2AH~V{!-Av|[3:gJd&p^`[bK4N*@g*Xu tW?n Yu(i8(6.k_[=d?O%#eaIKFPqK' );
define( 'LOGGED_IN_KEY',    'BG]}[Y_:(Yur) &<U)9lB7/HX|K@.E*SW?GKn;dBv47AXi4@&aVD?;CtUfLt{XYA' );
define( 'NONCE_KEY',        'hO_:09FZ4@XJ3DA_J-V$&)8UI>1mijRq}H]/]V89i[7|gFx0bS/y>[tQ2@X)Y7Wk' );
define( 'AUTH_SALT',        '3WH<}*^h6s`%ss)p3|_%F0uJ]NJcoS70q88^-h{+6o*}iz.HHeRl}2=?+@z_V*p6' );
define( 'SECURE_AUTH_SALT', '^!VfU6?u-j?|Pi@=ZwFkE-;+i)SO$_7?GD0#AD@t7J,Va0KC2Bu;=vG49t%OKWJ7' );
define( 'LOGGED_IN_SALT',   ']m*+/mjyf i*S$A)_]k|cZ` (fi?U@g#N=8|J;nB`3(;Cd];CS8t,uWdW-LqGpo3' );
define( 'NONCE_SALT',       'm;9VwRa?J{q`I(bP/1c!d.z@Ja,>%CJm?m]iEBEq`Np^IkG}rp+VmPNw.nTHrY}D' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'dn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
